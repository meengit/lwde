# LWDE

Lightweight WordPress Development Environment

## Structure

```bash
.
├── .env.example             # Example of the environment configuration
├── README.md                # This README.md
├── apache
│   ├── Dockerfile           # Dockerfile to build apache container
│   ├── apache.conf          # Apache configuration
│   └── ssl                  # SSL certificates
├── database
│   ├── mysql                # MariaDB Database
│   └── backup               # Database backup direcory
├── docker-compose.yml       # docker-compose file
├── httpdocs                 # WordPress PHP files
│   ├── (vendor)             # Vendor dir of composer
│   └── wordpress            # Web Root of WordPress
├── php
│   ├── Dockerfile.cli       # Dockerfile to build project cli
│   ├── Dockerfile.php       # Dockerfile to build php-fpm
│   ├── conf                 # Configuration for PHP and Extensions
│   └── log                  # PHP Log files
└── tools                    # A set of development tools
```

## Requirements

Docker and docker-compose: <https://www.docker.com/>

## Prepare the development stack

* Clone or download this repository
* Rename the file `.env.example` to `.env` and add your values
* Prepare the Apache Web Server
  * Add your SSL certificate in `apache/ssl`. If you don't know how to generate self-signed certificates, please refer to Wiki page "[Generate-Certificates](https://gitlab.com/meengit/lwde/-/wikis/SSL-Certificates)" and see the naming conventions in `apache/ssl/README.txt`.
  * Open `apache/apache.conf` and update the placeholders with your values. Be sure you have entered the same URL for the server name you use as the common name in your certificate!

## Build and run the environment

```bash
docker-compose up -d
```

## Install Wordpress

### Manually

Download the offical WordPress and place it in `httpdocs/wordpress`.

### Composer

The stack comes with initial support for <a href="https://github.com/johnpbloch/wordpress" target="_blank">johnpbloch/wordpress</a> and <a href="https://wpackagist.org/" target="_blank">Packagist</a>. To install WordPress using Composer, run:

```bash
docker-compose exec -T cli bash -c "cd /var/www/html && composer install"
```

## Enable URL redirection to local development environment

Add the web server URL to your hosts file:

```bash
127.0.0.1   www.yourdomain.com
```

You can find your computer's hosts file in `/etc/hosts` for UNIX and UNIX-like systems and in `C:\Windows\System32\Drivers\etc\hosts` on Microsoft Windows.

## docker-compose services

### cli

`cli` container providing access to [PHP Composer](https://getcomposer.org/) and [WP CLI](https://wp-cli.org/).

Usage:

```bash
docker-compose exec cli bash
```

### apache

Apache 2.X webserver handling your `HTTP` or `HTTPS` requests (Proxy passes requests to fpm).

The SSL certificates are stored in the directory `./apache/ssl` (please see `README.txt` in `apache/ssl`). Don't forget to add the root certificate of your domain certificates to the certificate manager of your operating system. For Firefox browsers you have to add it to the _certificate manager_ of Firefox (in preferences).

### php

`php-fpm` running the WordPress PHP application.

### mariadb

[MariaDB](https://mariadb.org/) Database server.

This service is using a dynamic port mapping between the host and the docker container. To get the currently assigned port run…

```bash
docker ps
```

…and find the mariadb container and its mapping, for instance:


```bash
CONTAINER ID        IMAGE              PORTS                   NAMES
03e2263d67c4        mariadb:10.3       0.0.0.0:32773->3306/tcp wordpress_mariadb_1
^
|
|
Local Port = 32773
```

So, you have to configure your database connection with `127.0.0.1:32773` – for instance to import the database backup:

```bash
cd database
mysql -h 127.0.0.1 -P 32773 -u <user> -p -D <database> < database/backup/db.sql
```

## Other Tools

To use the other tools in the `tools`-directory, copy and rename `tools/env.example.sh` to `tools/env.sh` and add your values. For usage, please refert to the comments in the script header of `tools/*.sh`.

## License

```text
Lightweight WordPress Development Environment (LWDE)
Copyright (C) 2022 Andreas, development@meen.ch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```

