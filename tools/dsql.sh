#!/usr/bin/env bash

#!/usr/bin/env bash

#===============================================================================
#
#          FILE: dsql
#
#         USAGE: ./dsql.sh
#
#   DESCRIPTION: Download database of production
#
#       OPTIONS: ---
#  REQUIREMENTS: Local: ssh, Remote: mysqldump
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Andreas
#  ORGANIZATION:
#       CREATED: 17 Jan 2021
#      REVISION:  ---
#===============================================================================

# Debugging
# set -o nounset                             # Treat unset variables as an error
# set -x                                     # Debug when it executs

if [ ! -f ./env.sh ]; then
    echo "Error: File 'env.sh' not found!"
    echo "Copy 'env.example.sh' to 'env.sh' first and enter your values."
    exit 1
fi

function dsql() {
  EXPORT_DATE=$(date +%s)

  ssh "$SSH_USER"@"$SSH_HOST" -p "$SSH_PORT" "
    cd ${SSH_REMOTE_DB_BACKUP_DIR}
    pwd
    echo 'Dumping database… Export Date: ${EXPORT_DATE}'
    mysqldump -h ${SSH_REMOTE_DB_HOST} -Q -u ${SSH_REMOTE_DB_USER} -p ${SSH_REMOTE_DB_NAME} --result-file=${EXPORT_DATE}.sql --verbose
  "
  rsync -azP -e "ssh -p ${SSH_PORT}" "$SSH_USER"@"$SSH_HOST":"$SSH_REMOTE_DB_BACKUP_DIR/" "${SSH_LOCAL_PROJECT_ROOT}/backups"
}

. env.sh && dsql
