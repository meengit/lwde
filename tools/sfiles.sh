#!/usr/bin/env bash

#===============================================================================
#
#          FILE: sfiles
#
#         USAGE: ./sfiles.sh
#
#   DESCRIPTION: Sync uploaded files (WP) from remote host to local dev. env
#
#       OPTIONS: ---
#  REQUIREMENTS: Local: rsync, ssh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Andreas
#  ORGANIZATION:
#       CREATED: 18 Jan 2021
#      REVISION:  ---
#===============================================================================

# Debugging
# set -o nounset                             # Treat unset variables as an error
# set -x                                     # Debug when it executs

if [ ! -f ./env.sh ]; then
    echo "Error: File 'env.sh' not found!"
    echo "Copy 'env.example.sh' to 'env.sh' first and enter your values."
    exit 1
fi

function sfiles(){
  rsync -azP -e "ssh -p ${SSH_PORT}" "$SSH_USER"@"$SSH_HOST":"$SSH_REMOTE_PROJECT_ROOT/docroot/wp-content/uploads/" "${SSH_LOCAL_PROJECT_ROOT}/httpdocs/wp-content/uploads"
}

. env.sh && sfiles
