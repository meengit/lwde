#!/usr/bin/env bash

#!/usr/bin/env bash

#===============================================================================
#
#          FILE: env.sh
#
#         USAGE: ./env.sh
#
#   DESCRIPTION: Set environment variables for service scripts.
#   
#                1. Copy and rename this file to "env.sh"
#                2. Set the values for your environment
#
#       OPTIONS: ---
#  REQUIREMENTS: Required by some of the service scripts in the tools folder.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Andreas
#  ORGANIZATION:
#       CREATED: 17 Jan 2021
#      REVISION:  ---
#===============================================================================

# Debugging
# set -o nounset                             # Treat unset variables as an error
# set -x                                     # Debug when it executs

export SSH_USER=""
export SSH_HOST=""
export SSH_PORT=""
export SSH_LOCAL_PROJECT_ROOT="Absolute path to project"
export SSH_REMOTE_PROJECT_ROOT="Relative dir from ssh login directory."

export SSH_REMOTE_DB_HOST=""
export SSH_REMOTE_DB_NAME=""
export SSH_REMOTE_DB_USER=""
export SSH_REMOVE_DB_BACKUP_DIR="Relative dir from ssh login directory."
