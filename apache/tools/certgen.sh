#!/usr/bin/env bash

#===============================================================================
#
#          FILE: certgen
#
#         USAGE: ./certgen.sh
#
#   DESCRIPTION: Generate self signed certificate(s)
#
#       OPTIONS: ---
#  REQUIREMENTS: openssl
#          BUGS: ---
#         NOTES: Script based on
#                https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate
#        AUTHOR: Andreas
#  ORGANIZATION:
#       CREATED: 21 Oct 2020
#      REVISION:  ---
#===============================================================================

# Debugging
# set -o nounset                             # Treat unset variables as an error
# set -x                                     # Debug when it executs

ROOTCERT=''

function createRootCert() {
  echo "Enter a name for your root certificate (without suffix):"
  read -r -p "> " ROOTCERT

  # Generate private key
  openssl genrsa -des3 -out "$ROOTCERT.key" 2048

  # Generate root certificate
  openssl req -x509 -new -nodes -key "$ROOTCERT.key" -sha256 -days 825 -out "$ROOTCERT.pem"
}

function createCAsignedCerts() {
  echo "Enter your domain:"
  read -r -p "> " NAME

  # Generate a private key
  openssl genrsa -out $NAME.key 2048

  # Create a certificate-signing request
  openssl req -new -key $NAME.key -out $NAME.csr

# Create a config file for the extensions
>$NAME.ext cat <<-EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = $NAME # Be sure to include the domain name here because Common Name is not so commonly honoured by itself
DNS.2 = www.$NAME # Optionally, add additional domains (I've added a subdomain here)
EOF
# optonally right above EOF
# IP.1 = 192.168.0.13 # Optionally, add an IP address (if the connection which you have planned requires it)


  # Create the signed certificate
  openssl x509 -req -in $NAME.csr -CA $ROOTCERT.pem -CAkey $ROOTCERT.key -CAcreateserial \
  -out $NAME.crt -days 825 -sha256 -extfile $NAME.ext
}

function main() {
  echo "Certs will be generated in $(pwd)"
  echo "Do you want to generate a root cert? [y|n]"
  read -r -p "> " INCLROOT

  if [ "$INCLROOT" = "y" ]; then
    createRootCert
    createCAsignedCerts
  else
      echo "Enter the path and name without suffix of your root certificate."
      echo "openssl will us a *.key and a *.pem file sharing the same name!"
      read -r -p "> " ROOTCERT
      createCAsignedCerts
  fi
}

main

