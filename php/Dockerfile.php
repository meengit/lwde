ARG PHP_VERSION=""
FROM php:${PHP_VERSION:+${PHP_VERSION}-}fpm-alpine AS runtime

RUN apk update; \
    apk upgrade;

RUN apk add -U bash git

# Typically, we don't need this because we use mlocati/docker-php-extension-installer.
#RUN apk add -U $PHPIZE_DEPS

# Please consider the table of supported extensions:
# https://github.com/mlocati/docker-php-extension-installer#supported-php-extensions.
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions imagick ssh2 exif gd mysqli pdo_mysql sockets xdebug zip

COPY ./conf/php.ini /usr/local/etc/php/php.ini
COPY ./conf/extensions/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini 

WORKDIR /var/www

CMD ["php-fpm", "-F"]
